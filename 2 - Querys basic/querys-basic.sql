
-- A continuacion se presentara y se pondra en practica algunas de las consultas basicas sobre cualquier
-- motor de base de datos.

-- Para las pruebas se utilizaran una estructura basica, conformada por clientes, plato y pedidos.
-- A cada pedido le corresponde una asociacion a un cliente y un plato.

-- Creacion de tablas

Create Table cliente
(
	idCliente Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    telefono varchar(30),
    PRIMARY KEY (idCliente)
);

Create Table plato
(
	idPlato Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    precio decimal (10,2),  -- 10 digitos y 2 decimales
    PRIMARY KEY (idPlato)
);

Create Table pedido
(
	idPedido Int NOT NULL AUTO_INCREMENT,
	detalle TEXT,
	fecha datetime,
    idPlato Int NOT NULL,
    idCliente Int,
    FOREIGN KEY (idPlato) REFERENCES plato (idPlato),
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
	PRIMARY KEY (idPedido)
);

-- Insercion de registros.

INSERT INTO cliente ( nombre, telefono )
VALUES 	( "Diego Maradona", "1010101010"),
	   	( "Ernesto Guevara", "1010101011"),
	   	( "Javier Ramos", "1010101100");

INSERT INTO plato ( nombre, precio )
VALUES 	( "Pizza", 2.52),
	   	( "Empanada", 5.36),
		( "Locro", 1.63),
	   	( "Milanesas con pure", 8.99);

INSERT INTO pedido ( detalle, fecha, idPlato, idCliente )
VALUES 	( "Lo quiere con papa fritas", '2016-10-23 20:44:11', 1, 1),
	   	( "Sin aderezos", '2017-10-23 20:44:11', 2, 1),
		( "Con pollo frito", '2018-10-23 20:44:11', 3, 2),
	   	( "Sin detalles", '2020-10-23 20:44:11', 4, 2),
	   	( "Sin detalles", '2021-10-23 20:44:11', 4, NULL);

-- WHERE - Obtener todos los datos de un cliente en particular.

SELECT * 
FROM cliente 
WHERE idCliente=1;

-- ORDER BY - ASC - DESC - Ordenar los pedidos ordenados por fecha

-- ascendente
SELECT * 
FROM pedido p
Order by p.fecha asc;

-- descendente
SELECT * 
FROM pedido p
Order by p.fecha desc;

-- GROUP BY - COUNT - AS - Obtener la cantidad de pedidos realizados por cada cliente.

SELECT c.nombre, COUNT(pe.idCliente) as cantidad_pedidos 
FROM pedido pe, cliente c
WHERE pe.idCliente = c.idCliente
GROUP by pe.idCliente;

-- SUM - Obtener la cantidad de pedidos realizados y el gasto acumulado de cada cliente

SELECT c.nombre, COUNT(pe.idCliente) as cantidad_pedidos, SUM(pl.precio) as gasto_acum
FROM pedido pe, cliente c, plato pl
WHERE pe.idCliente = c.idCliente and pe.IdPlato = pl.idPlato
GROUP by pe.idCliente;

-- INNER JOIN o JOIN - Obtener la cantidad de pedidos realizados por cada cliente.

SELECT c.nombre, COUNT(pe.idCliente) as cantidad_pedidos
FROM pedido pe
	JOIN cliente c
	ON pe.idCliente = c.idCliente
GROUP by pe.idCliente;

-- LEFT JOIN : Si no existe ninguna coincidencia para alguna de las filas de la tabla de 
-- la izquierda, de igual forma todos los resultados de la primera tabla se muestran.
-- Por ej. el pedido con id = 5.

SELECT *
FROM pedido pe
LEFT JOIN cliente c
ON pe.idCliente = c.idCliente

-- RIGHT JOIN : Misma idea pero del lado derecho (tabla cliente). En este caso en particular
-- hay un cliente (Javier Ramos) que no tiene ningun pedido. Por lo tanto mostrara, todos
-- los datos del cliente Javier y en los campos de pedido apareceran NULL.

SELECT *
FROM pedido pe
RIGHT JOIN cliente c
ON pe.idCliente = c.idCliente
