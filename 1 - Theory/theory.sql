-- Accedemos con credenciales de root.

mysql -u root -p 

-- Cambiamos la contraseña del usuario.

ALTER USER 'nombreUsuario'@'localhost' IDENTIFIED BY 'NuevaContraseña';

-- Crear una base de datos.

CREATE DATABASE <name_db>;

-- Usar una base de datos.

USE <name_db>;

-- Elimina la base de datos si existe.

DROP DATABASE IF EXISTS <name_db>;

-- Elimina la tabla.

DROP TABLE <name_tb>;

-- Creacion de una tabla.
-- En donde 'idContacto' en la pk. Unica (y auto incremental en este caso) para cada registro.

Create Table contacto
(
	idContacto Int NOT NULL AUTO_INCREMENT,
	nombre varchar(30),
    apellido varchar(30),
    telefono varchar(30),
    email varchar(30),
    PRIMARY KEY (idContacto)
);

-- Insertar registros a un tabla.

INSERT INTO contacto ( nombre, apellido, telefono, email )
VALUES ( "Diego" , "Maradona", "1010101010", "diego.maradona@gmail.com" ),
	   ( "Ernesto", "Guevara", "1010101011", "ernesto.guevara@gmail.com" );

-- Actualizar un registro.

UPDATE contato
SET nombre = "Javier"
WHERE idContacto = 1; 

-- Eliminar un registro.

DELETE FROM contacto WHERE idContacto = 1;

-- Exportar registros a un archivo csv.

SELECT
    idContacto,
    nombre,
    apellido,
    telefono,
    email
FROM contacto INTO OUTFILE '/tmp/contactos.csv';

-- Importar registros desde un archivo csv.

LOAD DATA LOCAL INFILE '/tmp/contactos.csv' INTO TABLE contacto LINES TERMINATED BY '\n';

-- Creacion de tablas relacionales: 1 --- *
-- En este caso tenemos que una promocion esta asociada a una y solo una, campania (No digo campaña porque la 'ñ' nos puede traer problemas).
-- Como dato a tener encuenta, la creacion de la tabla independiente (campania) debe crearse primero que la dependiente (promocion).
-- A la variable que conecta una tabla con la otra se la conoce como foreign key o fk, y es la pk de la tabla independiente.

Create Table campania
(
	idCampania Int NOT NULL AUTO_INCREMENT,
    numero varchar(30),
    anio varchar(30),
    cierre date,
    PRIMARY KEY (idCampania)
);

Create Table promocion
(
	idPromocion Int NOT NULL AUTO_INCREMENT,
    nombre varchar(30),
    descripcion varchar(100),
    pagina varchar(30),
    precio Int NOT NULL,
    idCampania Int NOT NULL,
    FOREIGN KEY (idCampania) REFERENCES campania (idCampania),
    PRIMARY KEY (idPromocion)
);

-- Creacion de tablas relacionales: * --- *
-- En este caso, necesitamos una tabla intermedia (C) que conecten con las tablas A y B, para podes hacer una relacion muchos a muchos.

Create Table A
(
	idA Int NOT NULL AUTO_INCREMENT,
    var1 varchar(30),
    var2 varchar(30),
    PRIMARY KEY (idA)
);

Create Table B
(
	idB Int NOT NULL AUTO_INCREMENT,
    var1 varchar(30),
    var2 varchar(30),
    PRIMARY KEY (idB)
);

Create Table C
(
	idC Int NOT NULL AUTO_INCREMENT,
    idA Int NOT NULL,
    idB Int NOT NULL,
    FOREIGN KEY (idA) REFERENCES A (idA),
    FOREIGN KEY (idB) REFERENCES B (idB),
    PRIMARY KEY (idC)
);
