
USE SCADA_System_Desktop;

-- ----------------------------------------------------------------------------------------------------------
-- CREATES OF TABLES --
-- ----------------------------------------------------------------------------------------------------------

CREATE TABLE dispositivo
(
  	id_dispositivo VARCHAR (36),
  	nombre_dispositivo VARCHAR (50),
	descripcion varchar (150),
	tipo varchar (3),
	ip varbinary (4),
	puerto int,
	nombre_puerto varchar (50),
	unit_id int,
	baud_rate int,
	data_bits int,
	parity varchar (10),
	stop_bits int,
	encoding varchar (3),
	habilitado boolean,
    PRIMARY KEY (id_dispositivo)
);

CREATE TABLE registro
(
  	id_registro varchar (36),
  	nombre_registro varchar (50),
	descripcion varchar (150),
	unidad varchar (25),
	funcion int,
	ref_registro_modbus int,
	ref_bit_coil int,
	operacion varchar (20),
	tamanio int,
	representacion varchar (30),
	habilitado boolean,
	id_dispositivo VARCHAR (36),
	FOREIGN KEY (id_dispositivo) REFERENCES dispositivo (id_dispositivo),
    PRIMARY KEY (id_registro)
);

-- ----------------------------------------------------------------------------------------------------------
-- DELETES OF RECORDS --
-- ----------------------------------------------------------------------------------------------------------

-- ALL
-- DELETE FROM registroLog WHERE fecha <= NOW();
DELETE FROM registro WHERE habilitado = true or habilitado = false;
DELETE FROM dispositivo WHERE tipo = "TCP" or tipo = "RTU";

-- ----------------------------------------------------------------------------------------------------------
-- INSERTION OF RECORDS --
-- ----------------------------------------------------------------------------------------------------------

INSERT INTO dispositivo ( id_dispositivo, nombre_dispositivo, descripcion, tipo, ip, puerto, nombre_puerto, unit_id, baud_rate, data_bits, parity, stop_bits, encoding, habilitado )
VALUES( "8a5d9181-d8ad-4452-ab93-52c5f4e8b6d4", "DEV1Ch", "Dispositivo de control 1 sobre la planta de produccion de chocolate", "RTU", null, null, "COM1", 1, 9600, 8, "none", 1, "RTU", true ),
	  ( "f2f35f54-22c6-47b4-b8cc-804624587647", "DEV1Tu", "Dispositivo de control 1 sobre la planta de produccion de turron", "RTU", null, null, "COM2", 2, 9600, 8, "none", 1, "RTU", false ),
	  ( "e30a29de-38c5-4b45-9d15-0fe59127b5b1", "DEV2Em", "Dispositivo de control 2 sobre la planta de empaquetado", "TCP", _binary 0x7F000001, 502, null, null, null, null, null, null, null, false );

INSERT INTO registro ( id_registro, nombre_registro, descripcion, unidad, funcion, ref_registro_modbus, ref_bit_coil, operacion, tamanio, representacion, habilitado, id_dispositivo )
VALUES( "472ea797-0afc-40bd-a0b0-37cda8a87879", "MPCh", "Mando de produccion de chocolate", "on/off", 1, 00001, 0, "Read/Write", 0, "Binary" , true, "8a5d9181-d8ad-4452-ab93-52c5f4e8b6d4" ),
	  ( "230db884-57ec-46bf-841a-42a5f67a48e6", "MeI", "Funcionamiento de mezcladora de ingredientes", "on/off", 2, 10001, 0, "Only read", 0, "Binary", true, "8a5d9181-d8ad-4452-ab93-52c5f4e8b6d4" ),
	  ( "e768cc46-db21-4c44-a00c-d34daa05eb5f", "CaP", "Cantidad de cajas de chocolate producidas", "unidades", 3, 30001, 0, "Only read", 1, "Unsigned Integer", true, "8a5d9181-d8ad-4452-ab93-52c5f4e8b6d4" ),
	  ( "4e4e90be-5b6f-4218-ba02-704f52565417", "VPCh", "Velocidad de produccion de chocolate", "%", 4, 40001, 0, "Read/Write", 1, "Unsigned Integer", true, "8a5d9181-d8ad-4452-ab93-52c5f4e8b6d4" ),
	  ( "d422a7f9-87e4-44fa-bab9-f0195ae66103", "PCT", "Cantidad de cajas de turron producidas", "unidades", 3, 30001, 0, "Only read", 1, "Unsigned Integer", true, "f2f35f54-22c6-47b4-b8cc-804624587647" ),
	  ( "568b87ed-8233-499f-b23b-a6d3d5fc392a", "PSu", "Los paquetes tienen un peso de 1 Tn y corresponde a 35.378 unidades de sufruit.", "unidades", 3, 30001, 0, "Only read", 1, "Unsigned Integer", true, "e30a29de-38c5-4b45-9d15-0fe59127b5b1" );

-- ----------------------------------------------------------------------------------------------------------

SELECT * FROM registro;

SELECT * FROM plc1_sen1;

-- ----------------------------------------------------------------------------------------------------------

ALTER DATABASE <NAME_DATABASE> CHARACTER SET utf8 COLLATE UTF8_GENERAL_CI;

ALTER TABLE <NAME_TABLE> CHARACTER SET utf8 COLLATE UTF8_GENERAL_CI;

-- ----------------------------------------------------------------------------------------------------------
