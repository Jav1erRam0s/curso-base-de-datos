---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
---------------------------------------------------------------
-- Punto a - Se deben hacer los insert para inicializar la tabla buffer_pool con 4 buffers.
---------------------------------------------------------------
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 5, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 6, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 7, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 8, '2018-09-19 17:23:57');
---------------------------------------------------------------
-- Punto b - Implementar 4 funciones en postgres.
---------------------------------------------------------------
-- ** Funcion **
--pick_frame_LRU() Debe retornar el numero de frame que se debe desalojar segun LRU
Create or replace function pick_frame_LRU() returns int
as
$$
Select BP.nro_frame
from bufferpool BP,
	(Select min(last_touch) as el_mas_viejo 
	 From bufferpool BP
	 Order By min(last_touch)) T1
Where BP.last_touch = T1.el_mas_viejo;
$$
language sql;
/* llamada a la funcion */
Select pick_frame_LRU() as frame_que_se_debe_desalojar_LRU;
-----------------------------------------------------
-- ** Funcion **
--pick_frame_MRU() Debe retornar el numero de frame que se debe desalojar segun MRU.
Create or replace function pick_frame_MRU() returns int
as
$$
Select BP.nro_frame
from bufferpool BP,
	(Select max(last_touch) as el_mas_reciente 
	 From bufferpool BP
	 Order By max(last_touch)) T1
Where BP.last_touch = T1.el_mas_reciente;
$$
language sql;
/* llamada a la funcion */
Select pick_frame_MRU() as frame_que_se_debe_desalojar_MRU;
-----------------------------------------------------
/* Funcion */
--pick_frame_139() Debe retornar el numero de frame que se debe desalojar segun LRU, pero antes de cada 
--solicitud se debe verificar si las ultimas N solicitudes fueron secuenciales (nros de pagina contiguos). 
--Si hubo N secuenciales, debe retornar el numero de frame segun MRU, y poner en cero el contador de secuenciales. 
--N es un porcentaje de la cantidad de buffers en el pool (por ejemplo N=50%).
drop table if exists Registros_page cascade;
create table Registros_page(
	idPage SERIAL PRIMARY KEY,
	nro_page int
);
insert into Registros_page (nro_page) values (1);
insert into Registros_page (nro_page) values (5);
insert into Registros_page (nro_page) values (2);
insert into Registros_page (nro_page) values (3);
				  
Create or replace function esSecuencial(nro_sec INTEGER) returns boolean
as	
$BODY$
DECLARE
  rec record;
  cont INTEGER;
  anterior INTEGER;
  posterior INTEGER;
BEGIN
		 anterior=0;
		 cont=0;
         for rec in SELECT * FROM Registros_page loop
			IF (anterior = 0)  THEN
				anterior=rec.nro_page;
				cont=cont+1;
			ELSE
				posterior = anterior+1;
				IF (posterior = rec.nro_page)  THEN
					cont=cont+1;
		            anterior = rec.nro_page;
				ELSE
					cont=1;
				END IF;
			END IF;
        end loop;
		IF(cont>nro_sec) THEN
			return true;
		ELSE
			return false;
		END IF;
END;
$BODY$
language 'plpgsql';
/* llamada a la funcion */
--Select esSecuencial(2) as secuencial;

Create or replace function pick_frame_139() returns int
as
$BODY$
DECLARE 
	--num int
BEGIN
	IF (esSecuencial(2))  THEN
		return pick_frame_MRU();
	ELSE
		return pick_frame_LRU();
	END IF;
END;
$BODY$
language 'plpgsql';
/* llamada a la funcion */
Select pick_frame_139() as frame_que_se_debe_desalojar_139;
---------------------------------------------------------------
-- Punto c - Ejecucion de get_disk_page()
---------------------------------------------------------------
--Se deben crear 3 trazas de paginas a leer de disco, y crear un script para ejecutar la funcion 
--get_disk_page() varias veces con todas las trazas de paginas a leer de disco. Por ejemplo:
--select get_disk_page (100);
--select get_disk_page (101);
--select get_disk_page (102);
--select get_disk_page (103);
--select get_disk_page (101);
--select get_disk_page (102);
--select get_disk_page (105);
--select get_disk_page (102);
--select get_disk_page (103);
--select get_disk_page (106);
--select get_disk_page (102);
--Este punto se debe hacer usando MRU , LRU y 139 se deben comparar todos los resultados. 
--En un caso, MRU debe ser mejor, en otro LRU debe ser mejor y en otro 139 debe ser mejor.
---------------------------------------------------------------
---------------------------------------------------------------
-- ** Funciones ** -- For -------> LRU
---------------------------------------------------------------
---------------------------------------------------------------
--get_disk_page() Debe retornar el nro_frame donde se encuentra la pagina de disco solicitada.
Create or replace function get_disk_page_LRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	fr = get_fr_by_page_LRU(nro_page);
	IF(fr = 0) THEN
		fr = get_page_from_disk_LRU(nro_page);
	ELSE
		RAISE NOTICE 'ACCESO A BUFFERPOLL PAGINA: %, FRAME: %',nro_page, fr;
		UPDATE bufferpool set last_touch = clock_timestamp() WHERE nro_frame = fr;
		insert into resultado_LRU (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, null, true);
	END IF;
	RETURN fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_fr_by_page_LRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	select nro_frame into fr from bufferpool where nro_disk_page = nro_page;
	IF NOT FOUND THEN
		return 0;
	ELSE
		update bufferpool set last_touch = clock_timestamp() where nro_disk_page = nro_page;
		return fr;
	END IF;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_free_frame_LRU() returns int
as
$$
DECLARE fr int ;
BEGIN
	select nro_frame into fr from bufferpool where libre = true order by nro_frame ;
	IF NOT FOUND THEN
		fr = pick_frame_LRU();
		--fr = pick_frame_MRU();
		--fr = pick_frame_139();
	END IF;
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_page_from_disk_LRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int = get_free_frame_LRU();
		desalojo int;
BEGIN
	PERFORM read_page_from_disk(fr,nro_page); --NO HACE NADA
	select nro_disk_page into desalojo from bufferpool where nro_frame = fr;
	UPDATE bufferpool set libre = false, dirty = false, nro_disk_page = nro_page, last_touch = clock_timestamp() WHERE nro_frame = fr;
	RAISE NOTICE 'ACCESO A DISCO CON REEMPLAZO, PAG: %, FRAME: %, DESALOJO: %', nro_page, fr, desalojo;
	insert into resultado_LRU (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, desalojo, false);
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
---------------------------------------------------------------
-- ** Funciones ** -- For -------> MRU
---------------------------------------------------------------
---------------------------------------------------------------
--get_disk_page() Debe retornar el nro_frame donde se encuentra la pagina de disco solicitada.
Create or replace function get_disk_page_MRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	fr = get_fr_by_page_MRU(nro_page);
	IF(fr = 0) THEN
		fr = get_page_from_disk_MRU(nro_page);
	ELSE
		RAISE NOTICE 'ACCESO A BUFFERPOLL PAGINA: %, FRAME: %',nro_page, fr;
		UPDATE bufferpool set last_touch = clock_timestamp() WHERE nro_frame = fr;
		insert into resultado_MRU (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, null, true);
	END IF;
	RETURN fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_fr_by_page_MRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	select nro_frame into fr from bufferpool where nro_disk_page = nro_page;
	IF NOT FOUND THEN
		return 0;
	ELSE
		update bufferpool set last_touch = clock_timestamp() where nro_disk_page = nro_page;
		return fr;
	END IF;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_free_frame_MRU() returns int
as
$$
DECLARE fr int ;
BEGIN
	select nro_frame into fr from bufferpool where libre = true order by nro_frame ;
	IF NOT FOUND THEN
		fr = pick_frame_MRU();
	END IF;
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_page_from_disk_MRU(nro_page INTEGER) returns int
as
$$
DECLARE fr int = get_free_frame_MRU();
		desalojo int;
BEGIN
	PERFORM read_page_from_disk(fr,nro_page); --NO HACE NADA
	select nro_disk_page into desalojo from bufferpool where nro_frame = fr;
	UPDATE bufferpool set libre = false, dirty = false, nro_disk_page = nro_page, last_touch = clock_timestamp() WHERE nro_frame = fr;
	RAISE NOTICE 'ACCESO A DISCO CON REEMPLAZO, PAG: %, FRAME: %, DESALOJO: %', nro_page, fr, desalojo;
	insert into resultado_MRU (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, desalojo, false);
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
---------------------------------------------------------------
-- ** Funciones ** -- For -------> 139
---------------------------------------------------------------
---------------------------------------------------------------
-- ** Funciones **
--get_disk_page() Debe retornar el nro_frame donde se encuentra la pagina de disco solicitada.
Create or replace function get_disk_page_139(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	insert into Registros_page (nro_page) values (nro_page);
	fr = get_fr_by_page_139(nro_page);
	IF(fr = 0) THEN
		fr = get_page_from_disk_139(nro_page);
	ELSE
		RAISE NOTICE 'ACCESO A BUFFERPOLL PAGINA: %, FRAME: %',nro_page, fr;
		UPDATE bufferpool set last_touch = clock_timestamp() WHERE nro_frame = fr;
		insert into resultado_139 (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, null, true);
	END IF;
	RETURN fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_fr_by_page_139(nro_page INTEGER) returns int
as
$$
DECLARE fr int;
BEGIN
	select nro_frame into fr from bufferpool where nro_disk_page = nro_page;
	IF NOT FOUND THEN
		return 0;
	ELSE
		update bufferpool set last_touch = clock_timestamp() where nro_disk_page = nro_page;
		return fr;
	END IF;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_free_frame_139() returns int
as
$$
DECLARE fr int ;
BEGIN
	select nro_frame into fr from bufferpool where libre = true order by nro_frame ;
	IF NOT FOUND THEN
		fr = pick_frame_139();
	END IF;
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
Create or replace function get_page_from_disk_139(nro_page INTEGER) returns int
as
$$
DECLARE fr int = get_free_frame_139();
		desalojo int;
BEGIN
	PERFORM read_page_from_disk(fr,nro_page); --NO HACE NADA
	select nro_disk_page into desalojo from bufferpool where nro_frame = fr;
	UPDATE bufferpool set libre = false, dirty = false, nro_disk_page = nro_page, last_touch = clock_timestamp() WHERE nro_frame = fr;
	RAISE NOTICE 'ACCESO A DISCO CON REEMPLAZO, PAG: %, FRAME: %, DESALOJO: %', nro_page, fr, desalojo;
	insert into resultado_139 (nro_page, nro_frame, nro_page_desalojo, read_buffer) values (nro_page, fr, desalojo, false);	
	return fr;
END;
$$
language 'plpgsql';
---------------------------------------------------------------
-- PLANTEO DE CASOS -------------------------------------------
---------------------------------------------------------------					 
---------------------------------------------------------------
-- MEJOR CASO ---> LRU
---------------------------------------------------------------
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
---------------------------------------------------------------
drop table if exists resultado_LRU cascade;
---------------------------------------------------------------
create table resultado_LRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(2) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(4) as frame_donde_se_encuentra;
Select get_disk_page_LRU(5) as frame_donde_se_encuentra;
Select get_disk_page_LRU(4) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');				 
---------------------------------------------------------------
drop table if exists resultado_MRU cascade;
---------------------------------------------------------------
create table resultado_MRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(2) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(4) as frame_donde_se_encuentra;
Select get_disk_page_MRU(5) as frame_donde_se_encuentra;
Select get_disk_page_MRU(4) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
drop table if exists resultado_139 cascade;
drop table if exists Registros_page cascade;	
---------------------------------------------------------------
create table Registros_page(
	idPage SERIAL PRIMARY KEY,
	nro_page int
);
create table resultado_139(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(2) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(4) as frame_donde_se_encuentra;
Select get_disk_page_139(5) as frame_donde_se_encuentra;
Select get_disk_page_139(4) as frame_donde_se_encuentra;
---------------------------------------------------------------					 
Select * from resultado_LRU; --> Mejor Caso
Select * from resultado_MRU;
Select * from resultado_139;
---------------------------------------------------------------
---------------------------------------------------------------
-- MEJOR CASO ---> MRU
---------------------------------------------------------------
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
---------------------------------------------------------------
drop table if exists resultado_LRU cascade;
---------------------------------------------------------------
create table resultado_LRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(5) as frame_donde_se_encuentra;
Select get_disk_page_LRU(7) as frame_donde_se_encuentra;
Select get_disk_page_LRU(9) as frame_donde_se_encuentra;
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');				 
---------------------------------------------------------------
drop table if exists resultado_MRU cascade;
---------------------------------------------------------------
create table resultado_MRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(5) as frame_donde_se_encuentra;
Select get_disk_page_MRU(7) as frame_donde_se_encuentra;
Select get_disk_page_MRU(9) as frame_donde_se_encuentra;
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
drop table if exists resultado_139 cascade;
drop table if exists Registros_page cascade;	
---------------------------------------------------------------
create table Registros_page(
	idPage SERIAL PRIMARY KEY,
	nro_page int
);
create table resultado_139(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(5) as frame_donde_se_encuentra;
Select get_disk_page_139(7) as frame_donde_se_encuentra;
Select get_disk_page_139(9) as frame_donde_se_encuentra;
Select get_disk_page_139(1) as frame_donde_se_encuentra;
---------------------------------------------------------------					 
Select * from resultado_LRU; 
Select * from resultado_MRU; --> Mejor Caso
Select * from resultado_139;
---------------------------------------------------------------					 
---------------------------------------------------------------
-- MEJOR CASO ---> 139
---------------------------------------------------------------
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
---------------------------------------------------------------
drop table if exists resultado_LRU cascade;
---------------------------------------------------------------
create table resultado_LRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------					 
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(2) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(4) as frame_donde_se_encuentra;
Select get_disk_page_LRU(5) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(2) as frame_donde_se_encuentra;
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(6) as frame_donde_se_encuentra;
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');				 
---------------------------------------------------------------
drop table if exists resultado_MRU cascade;
---------------------------------------------------------------
create table resultado_MRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(2) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(4) as frame_donde_se_encuentra;
Select get_disk_page_MRU(5) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(2) as frame_donde_se_encuentra;
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(6) as frame_donde_se_encuentra;
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
drop table if exists resultado_139 cascade;
drop table if exists Registros_page cascade;	
---------------------------------------------------------------
create table Registros_page(
	idPage SERIAL PRIMARY KEY,
	nro_page int
);
create table resultado_139(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(2) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(4) as frame_donde_se_encuentra;
Select get_disk_page_139(5) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(2) as frame_donde_se_encuentra;
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(6) as frame_donde_se_encuentra;
Select get_disk_page_139(1) as frame_donde_se_encuentra;
---------------------------------------------------------------					 
Select * from resultado_LRU; 
Select * from resultado_MRU; 
Select * from resultado_139; --> Mejor Caso	
---------------------------------------------------------------	