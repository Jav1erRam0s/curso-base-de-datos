---------------------------------------------------------------					 
---------------------------------------------------------------
-- MEJOR CASO ---> 139
---------------------------------------------------------------
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
---------------------------------------------------------------
drop table if exists resultado_LRU cascade;
---------------------------------------------------------------
create table resultado_LRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------					 
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(2) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(4) as frame_donde_se_encuentra;
Select get_disk_page_LRU(5) as frame_donde_se_encuentra;
Select get_disk_page_LRU(3) as frame_donde_se_encuentra;
Select get_disk_page_LRU(2) as frame_donde_se_encuentra;
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
Select get_disk_page_LRU(6) as frame_donde_se_encuentra;
Select get_disk_page_LRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');				 
---------------------------------------------------------------
drop table if exists resultado_MRU cascade;
---------------------------------------------------------------
create table resultado_MRU(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(2) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(4) as frame_donde_se_encuentra;
Select get_disk_page_MRU(5) as frame_donde_se_encuentra;
Select get_disk_page_MRU(3) as frame_donde_se_encuentra;
Select get_disk_page_MRU(2) as frame_donde_se_encuentra;
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
Select get_disk_page_MRU(6) as frame_donde_se_encuentra;
Select get_disk_page_MRU(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
drop table if exists bufferpool cascade;
---------------------------------------------------------------
create table bufferpool(
	nro_frame SERIAL PRIMARY KEY,
	libre boolean,
	dirty boolean,
	nro_disk_page int,
	last_touch timestamp
);
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:59');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:52');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:55');
insert into bufferpool (libre, dirty, nro_disk_page, last_touch) values (true, false, 0, '2018-09-19 17:23:57');
---------------------------------------------------------------
drop table if exists resultado_139 cascade;
drop table if exists Registros_page cascade;	
---------------------------------------------------------------
create table Registros_page(
	idPage SERIAL PRIMARY KEY,
	nro_page int
);
create table resultado_139(
	id_resultado SERIAL PRIMARY KEY,
	nro_page int,
	nro_frame int,
	nro_page_desalojo int,
	read_buffer boolean
);
---------------------------------------------------------------
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(2) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(4) as frame_donde_se_encuentra;
Select get_disk_page_139(5) as frame_donde_se_encuentra;
Select get_disk_page_139(3) as frame_donde_se_encuentra;
Select get_disk_page_139(2) as frame_donde_se_encuentra;
Select get_disk_page_139(1) as frame_donde_se_encuentra;
Select get_disk_page_139(6) as frame_donde_se_encuentra;
Select get_disk_page_139(1) as frame_donde_se_encuentra;
---------------------------------------------------------------
Select * from resultado_LRU;
Select * from resultado_MRU;
Select * from resultado_139; --> Mejor Caso	
---------------------------------------------------------------	