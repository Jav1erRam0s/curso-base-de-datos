drop table if exists Continente cascade;
drop table if exists Frontera cascade;
drop table if exists Pais cascade;
drop table if exists Censo cascade;

---------------------------------------------------------------
-- Punto 1 - Creacion de tablas.
---------------------------------------------------------------

create table Continente(
	continente_id SERIAL PRIMARY KEY,
	Nombre varchar(20) NOT NULL
);

create table Pais(
	pais_id SERIAL PRIMARY KEY,
	nombre varchar(50),
	independencia date NOT NULL,
	continente_id INTEGER,
	Forma_de_gobierno varchar(50),
	poblacion int,
	FOREIGN KEY (continente_id) REFERENCES Continente (continente_id)
);

create table Censo(
	pais_id INTEGER,
	anio int,
	poblacion int,
	FOREIGN KEY (pais_id) REFERENCES Pais (pais_id)
);

create table Frontera(
	pais_id1 INTEGER,
	pais_id2 INTEGER,
	extension_km int,
	FOREIGN KEY (pais_id1) REFERENCES Pais (pais_id),
	FOREIGN KEY (pais_id2) REFERENCES Pais (pais_id)
);

---------------------------------------------------------------
-- Punto 2.1 - Insertar registros con archivo.csv
---------------------------------------------------------------

copy Continente (continente_id, Nombre) from 'C:\Datos_paises - Continente.csv' delimiter ',' csv header;
copy Pais (pais_id, Nombre,independencia,continente_id,Forma_de_gobierno,poblacion) from 'C:\Datos_paises - Pais.csv' delimiter ',' csv header;
copy Frontera (pais_id1, pais_id2, extension_km) from 'C:\Datos_paises - frontera.csv' delimiter ',' csv header;
copy Censo (pais_id, anio, poblacion) from 'C:\Datos_paises - Censo.csv' delimiter ',' csv header;

---------------------------------------------------------------
-- Punto 2.2 - Cargar los datos
---------------------------------------------------------------

-- Grupo 1: Done. ////////////////////////////////////////////

---------------------------------------------------------------
-- Punto 3 - Funcion: Coeficiente de variacion de poblacion
---------------------------------------------------------------

/* Funcion */
create or replace function get_pop_variation_rate (pais_analizado INTEGER)  returns float
as
$$
Select  cast(((((pobla_ult - pobla_anteult)*100)/pobla_anteult)+100) as float)/100
from (Select nombre,poblacion as pobla_ult 
	  from (Select p.nombre, c.anio, c.poblacion 
		   from pais p, censo c 
		   where p.pais_id = c.pais_id
		   and p.pais_id = pais_analizado
		   ORDER BY c.anio DESC limit 2) ultimos_dos_censos
	  ORDER BY anio DESC limit 1)  T1,
	 (Select nombre,poblacion as pobla_anteult 
	  from (Select p.nombre, c.anio, c.poblacion 
		   from pais p, censo c 
		   where p.pais_id = c.pais_id
		   and p.pais_id = pais_analizado
		   ORDER BY c.anio DESC limit 2) ultimos_dos_censos
	  ORDER BY anio ASC limit 1 ) T2
$$
language sql;
/* llamada a la funcion */
Select get_pop_variation_rate(1) as Coeficiente_de_Variacion_de_Poblacion;

---------------------------------------------------------------
-- Punto 4 - Vista: return: pa?s, poblacion de ultimo censo, y la poblaci?n actual.
---------------------------------------------------------------

drop view if exists viewPaisPoblacionCenso;
/* Vista */
create or replace View viewPaisPoblacionCenso 
as
Select TPais.nombre as nombre_pais, TCenso.poblacion as pobla_Censo_Ult, TCenso.anio as ult_censo, (((TCenso.poblacion * get_pop_variation_rate(TPais.pais_id))-TCenso.poblacion)*((2018-TCenso.anio)*0.1))+TCenso.poblacion  as pobla_estimada, 2018 as anio_actual
from	
	(Select max(anio) as ciclo, pais_id as pais
	from censo c
	group by pais_id) TComun,
	pais as TPais,
	censo as TCenso
where 
	TComun.ciclo = TCenso.anio and
	TComun.pais = TPais.pais_id and
	TCenso.pais_id = TPais.pais_id;
/* llamada a la vista */
Select * from viewPaisPoblacionCenso;

---------------------------------------------------------------
-- Punto 5 - Pregunta.
---------------------------------------------------------------

-- El la tabla "Pais", el atributo cantidad de poblacion representa 
-- la poblacion actual.
-- ademas, cantidad de poblacion tambien se repite en la tabla "Censo"
-- pero este representa con exactitud, la poblacion en el a?o especificado.

---------------------------------------------------------------
-- Punto 6 - Funcion: Poblaci?n actual estimada de un continente.
---------------------------------------------------------------

/* Funcion */
Create or replace function get_pop_by_continent(continente_analizado INTEGER) returns BIGINT 
as
$$
Select  cast(Sum((((TCenso.poblacion * get_pop_variation_rate(TPais.pais_id))-TCenso.poblacion)*((2018-TCenso.anio)*0.1))+TCenso.poblacion) as BIGINT)
from	
	(Select max(anio) as ciclo, pais_id as pais
	from censo c
	group by pais_id) as TComun,
	pais as TPais,
	censo as TCenso
where 
	TComun.ciclo = TCenso.anio and
	TComun.pais = TPais.pais_id and
	TCenso.pais_id = TPais.pais_id and
	TPais.continente_id = continente_analizado
$$
language sql;
/* llamada a la funcion */
Select get_pop_by_continent(1) as Poblacion_Estimada_del_Continente;

---------------------------------------------------------------
-- Punto 7 - Agregar una claves primarias a todas las tablas.
---------------------------------------------------------------

drop table if exists Continente cascade;
drop table if exists Frontera cascade;
drop table if exists Pais cascade;
drop table if exists Censo cascade;

create table Continente(
	continente_id SERIAL,
	Nombre varchar(20) NOT NULL
);

create table Pais(
	pais_id SERIAL,
	nombre varchar(50),
	independencia date NOT NULL,
	continente_id INTEGER,
	Forma_de_gobierno varchar(50),
	poblacion int
);

create table Censo(
	pais_id INTEGER,
	anio int,
	poblacion int
);

create table Frontera(
	pais_id1 INTEGER,
	pais_id2 INTEGER,
	extension_km int
);

ALTER TABLE Continente 
   ADD PRIMARY KEY (continente_id);
   
ALTER TABLE Pais 
   ADD PRIMARY KEY (pais_id);

---------------------------------------------------------------
-- Punto 8 - Agregar una clave for?nea a la tabla de pa?ses apuntando a continente.
---------------------------------------------------------------

ALTER TABLE Pais 
   ADD FOREIGN KEY (continente_id) 
   REFERENCES Continente (continente_id);

ALTER TABLE Censo 
   ADD FOREIGN KEY (pais_id) 
   REFERENCES Pais (pais_id);

ALTER TABLE Frontera
   ADD FOREIGN KEY (pais_id1) 
   REFERENCES Pais (pais_id),
   ADD FOREIGN KEY (pais_id2) 
   REFERENCES Pais (pais_id);
   
---------------------------------------------------------------
-- Punto 9 - 
---------------------------------------------------------------

Select p1.nombre, p2.nombre, f.extension_km From Pais p1, Pais p2, Frontera f
Where f.pais_id1 = p1.pais_id and f.pais_id2 = p2.pais_id
Order by f.extension_km desc;

---------------------------------------------------------------
-- Punto 10 - 
---------------------------------------------------------------

/* join entre paises */
Select p1.nombre as pais_1, p2.nombre as pais_2, p3.nombre as pais_3 
From Pais p1, Pais p2, Pais p3, Frontera f1, Frontera f2, Frontera f3
Where f1.pais_id1 = p1.pais_id and f1.pais_id2 = p2.pais_id
and f2.pais_id1 = p3.pais_id and f2.pais_id2 = p1.pais_id
and f3.pais_id1 = p3.pais_id and f3.pais_id2 = p2.pais_id;

/* join entre fronteras */
Select Tpais1.nombre as pais_1, Tpais2.nombre as pais_2, Tpais3.nombre as pais_3 
From
	(Select f1.pais_id1 as idPais1, f3.pais_id1 as idPais2, f3.pais_id2 as idPais3 
	From Frontera f1, Frontera f2, Frontera f3
	Where f1.pais_id1 = f2.pais_id1 
	and f3.pais_id1 = f2.pais_id2 
	and f3.pais_id2 = f1.pais_id2) as TFronteras,
	Pais as TPais1,
	Pais as TPais2,
	Pais as TPais3
Where
	TPais1.pais_id = TFronteras.idPais1 and
	TPais2.pais_id = TFronteras.idPais2 and
	TPais3.pais_id = TFronteras.idPais3;
	
---------------------------------------------------------------
-- Punto 11 - 
---------------------------------------------------------------

/* indice por frontera */
drop index if exists indiceFrontera;
create index indiceFrontera on Frontera("pais_id1","pais_id2");

---------------------------------------------------------------
-- Ver tablas 
---------------------------------------------------------------

Select * from Pais;
Select * from Continente;
Select * from Frontera;
Select * from Censo;